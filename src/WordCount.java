import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Scanner;

public class WordCount extends Thread {
	private File file;
	private Counter counter;
	private int fileCount = 0;
	
	public WordCount(String fileName, Counter totalCount) {
		file = new File(fileName);
		counter = totalCount;
	}
	
	public void run() {
		Scanner scanner;
		try {
			scanner = new Scanner(new FileInputStream(file));
			
			while (scanner.hasNext()) {
				String word = scanner.next();
				
				if (word.indexOf("\\") == -1) {
					fileCount++;
					counter.increment();
				}
			}
			scanner.close();
			
			System.out.println(file.getName() + ": " + fileCount);
		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		}
	}
	
	public static void main(String[] args) throws InterruptedException {
		Counter counter = new Counter();
		
		Thread thread1 = new Thread(new WordCount("concurrency.txt", counter));
		Thread thread2 = new Thread(new WordCount("threads.txt", counter));
		
		thread1.start();
		thread2.start();
		
		thread1.join();
		thread2.join();
		
		System.out.println("Total word count: " + counter.count);
	}
}

class Counter {
	int count = 0;
	int threadCount = Thread.activeCount();
	public synchronized void increment() {
		count++;
		threadCount = Thread.activeCount();
	}
}